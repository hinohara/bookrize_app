class AddOpenbdToPosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :picture, :string
    add_column :posts, :publisher, :text
    add_column :posts, :author, :text
    add_column :posts, :pubdate, :text
    add_column :posts, :description, :text
  end
end
